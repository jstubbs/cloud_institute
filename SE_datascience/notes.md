1) Start the mysql db with docker-compose up.

2) docker exec into container and run the load the data:
   $ cd /var/lib/mysql-files
   $ mysql -u root -pcloud@tacc4ever < sql_import.sql

3) Look in the mysql db:
   mysql -u root -pcloud@tacc4ever

4) make a python3 virtualenv:
   $ virtualenv -p python3 cloudinst
   $ source cloudinst/bin/activate
   $ pip install -r requirements.txt


5) Questions we can answer:
  a) Count total number of questions with an answer, with an accepted answer.
  b) Count total number of unique users with an accepted answer.
  c) Count accepted answers by day of week
  d) Count number/percentage of questions answered within one hour of posting.
  e) Find probability of question having an accepted answer if it doesn't have one within i) one hour? ii) one day? iii) one week?
  f) Compare some of the analyses above for each SE site.
  g) Group the SE sites into "groups" of sites (e.g., recreational, hard science, computer science). Do

6) Some SE sites we could use and their compressed file sizes:

    es.stackoverflow.com.7z  73.7M
    cs.stackexchange.com.7z  51.1M
    astronomy.stackexchange.com.7z   13.2M
    biology.stackexchange.com.7z  40.7M
    chemistry.stackexchange.com.7z  47.5M
    engineering.stackexchange.com.7z  11.3M
    askubuntu.com.7z   546.7M

    beer.stackexchange.com.7z  2.2M
    bicycles.stackexchange.com.7z  31.5M
    chess.stackexchange.com.7z    10.8M
    cooking.stackexchange.com.7z   45.3M

7) fields:
  Id, PostTypeId, AcceptedAnswerId,