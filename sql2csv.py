import os

import dask.dataframe as dd
import MySQLdb
import pandas as pd


# mysql connectivity: host, port, username and password
mysql_user = "root"
mysql_password = "cloud@tacc4ever"
mysql_host = "127.0.0.1"
mysql_port = 3306


# database names and corresponding mysql connections
dbs = [
    {"name": "datascience",
     "con": MySQLdb.connect(user=mysql_user, passwd=mysql_password, host=mysql_host, port=mysql_port, db="datascience"), },
    # {"name": "stackoverflow",
    # "con": MySQLdb.connect(user=mysql_user, passwd=mysql_password, host=mysql_host, port=mysql_port, db="stackoverflow"), },
]

# whether to actually generate the csv files
generate_csv = False

# the SQL query to pull questions from the SE site database.
questions_query = """
SELECT id, 
       owneruserid, 
       posttypeid, 
       creationdate, 
       Dayofweek(creationdate) AS CreationDayOfWeek, 
       acceptedanswerid, 
       score, 
       answercount, 
       viewcount, 
       owneruserid, 
       lasteditoruserid, 
       lasteditdate, 
       LastActivityDate, CommentCount, FavoriteCount    from posts where PostTypeId=1;
"""

# the SQL query to pull accepted answers for each question post
answers_query = """
SELECT id                      AS acceptedAnswerId, 
       creationdate            AS acceptedAnswerCreationDate, 
       Dayofweek(creationdate) AS acceptedAnswerCreationDayOfWeek, 
       score                   AS acceptedAnswerScore, 
       commentcount            AS acceptedAnswerCommentCount, 
       owneruserid             AS acceptedAnswerOwnerUserId 
FROM   posts 
WHERE  id IN (SELECT acceptedanswerid 
              FROM   posts 
              WHERE  posttypeid = 1 
                     AND acceptedanswerid IS NOT NULL); 
"""


def do_query(db_con, query):
    """Execute an SQL query, `query`,  using a MySQL db connection, `db_con`."""
    cur = db_con.cursor()
    cur.execute(query)
    return cur.fetchall()

def get_basic_df(db, q):
    """Return a pandas df populated using query, `q`, from the `db` database."""
    df_qs = pd.read_sql(q, con=db["con"])

    # add the site column equal to the db name
    df_qs['site'] = db["name"]
    return df_qs

def get_site_df(db):
    """Return a pandas df with all rows for a single SE site."""
    df_qs = get_basic_df(db, questions_query)
    df_as = get_basic_df(db, answers_query)
    return pd.concat([df_qs, df_as], axis=1)


def df_to_csv(df, name, dir_path=None):
    """Write the df, `df` to a CSV file at path `path`."""
    if not dir_path:
        dir_path = os.getcwd()
    path = os.path.join(dir_path, name)
    df.to_csv(path)

def test_dask(csv):
    """Test the validity of a csv file at absolute path `csv` by importing it to a dask df."""
    # read the csv into a dask datafram
    df = dd.read_csv(csv)

    # compute the average number of comments by site
    df.groupby(df.site).CommentCount.mean().compute()

    # compute the average number of answers by day of the week
    df.groupby(df.CreationDayOfWeek).AnswerCount.mean().compute()


# generate the CSV files for each site in the dbs list.
if generate_csv:
    for db in dbs:
        df = get_site_df(db)
        df_to_csv(df, '{}.csv'.format(db['name']))